import 'package:flutter_test/flutter_test.dart';
import 'package:tsp/backend/notifications.dart';

void main(){
  Notifications notifications;

  setUp((){
    notifications = new Notifications();
  });

  test("Debe regresar un boolean true si la alerta del exceso de velocidad se lanzó satisfactoriamente", () async{
    var _notification = await notifications.notificacionExcesoVelocidad();
    expect(true, _notification);
  });


  test("Debe regresar un boolean true si la alerta del exceso de volumen se lanzó satisfactoriamente", () async{
    var _notification = await notifications.notificacionVolumenAlto();
    expect(true, _notification);
  });

}