import 'package:flutter_test/flutter_test.dart';
import 'package:tsp/backend/data_get_api.dart' as dataGet;

void main(){
  test('Debe regresar un String con la velocidad en esas coordenadas', () async {
    String lat = '22.772397';
    String long = '-102.562118';
    String func = await dataGet.DataGetApi().calculaPlaceId(lat, long);
    expect(func, "40");
  });

  test('Debe regresar un error al no pasar coordenadas validas', () async {
    String lat = '22.772397';
    String long = '';
    String func = await dataGet.DataGetApi().calculaPlaceId(lat, long);
    expect(func, "No fue posible obtener el limite de velocidad en este segmento");
  });
}