import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:geolocator/geolocator.dart';
import 'package:tsp/backend/current_location.dart';
import 'package:location_permissions/location_permissions.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  LocationLive location;
  Stream loc;

  setUp(() async {
    location = new LocationLive();
    location.LocationLiveM();
    loc = location.getStream;
  });

  test('Obtener y escuchar el stream de posición actual', () async {
    var sink = location.getSink;
    sink.add("variable1");
    sink.add("variable2");
    sink.add("variable3");
    sink.close();

    expect(loc, emitsInOrder(["variable1", "variable2", "variable3"]));
  });

   test('Obtener y escuchar el stream de posición en tiempo real', () {

  });

  test('Debe regresar un Color amarillo si la diferencia velocidad está entre 10 y -10', (){
    Color func = location.warningAlert(35, 30);
    expect(func, Colors.yellow);
  });

  test('Debe regresar un Color rojo si la diferencia de velocidad está menor a -10', (){
    Color func = location.warningAlert(41, 30);
    expect(func, Colors.red);
  });

  test('Debe regresar un Color negro si la diferencia de velocidad está mayor a 10', (){
  Color func = location.warningAlert(15, 30);
  expect(func, Colors.black);
  });

}
