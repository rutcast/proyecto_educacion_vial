import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:tsp/backend/notifications.dart';

class ConfiguracionMain extends StatefulWidget {

  final Notifications notifications;

  const ConfiguracionMain ({ Key key, this.notifications }): super(key: key);

  @override
  _ConfiguracionMain createState() => _ConfiguracionMain();
}

class _ConfiguracionMain extends State<ConfiguracionMain> {
  
  final _tiempoMantenimientoController = TextEditingController();
  int _tiempoRestante;

  @override
  void initState() {
    super.initState();
    _loadSharedPreferences();
  }

  @override
  void dispose() {
    _tiempoMantenimientoController.dispose();
    super.dispose();
  }

  void _loadSharedPreferences() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    int diasTranscurridos = await _getDiasTranscurridos();
    _tiempoMantenimientoController.text = pref.getInt("tiempoMantenimiento").toString();
    _tiempoRestante = pref.getInt("tiempoMantenimiento") - diasTranscurridos; 

    if(_tiempoRestante <= 0) {
      await pref.setInt("ultimoMantenimiento", DateTime.now().millisecondsSinceEpoch);
      _saveSharedPreferences(pref.getInt("tiempoMantenimiento"));
      diasTranscurridos = await _getDiasTranscurridos();
      _tiempoRestante = pref.getInt("tiempoMantenimiento") - diasTranscurridos; 
    }
    
    setState(() {});
  }

  Future<int> _getDiasTranscurridos() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return Duration(milliseconds: DateTime.now().millisecondsSinceEpoch).inDays - 
      Duration(milliseconds: pref.getInt("ultimoMantenimiento")).inDays;
  }

  void _saveSharedPreferences(int dias) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setInt("tiempoMantenimiento",  dias);
    _tiempoRestante = pref.getInt("tiempoMantenimiento");
    widget.notifications.notificacionMantenimiento(int.parse(_tiempoMantenimientoController.text));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.blue[400],
        title: Text('Configuración'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: TextField(
                      decoration: new InputDecoration(labelText: "Días para notificar mantenimiento"),
                      controller: _tiempoMantenimientoController,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                    ),
                  ),
                  Text("Restante: " + _tiempoRestante.toString())
                ]
              ),
            ),
            Center(
              child: RaisedButton(
                onPressed: () {
                  _saveSharedPreferences(int.parse(_tiempoMantenimientoController.text));
                  Toast.show("¡Guardado!", context, duration: Toast.LENGTH_SHORT, gravity: Toast.CENTER);
                }, 
                child: Text("Guardar"), 
                color: Colors.blue[400], 
                textColor: Colors.white,
              ),
            ),
          ],
        ),
      )
    );
  }
}

