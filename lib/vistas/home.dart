import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tsp/backend/current_location.dart';
import 'package:tsp/backend/data_get_api.dart';
import 'package:tsp/vistas/speed_meter.dart';
import 'package:toast/toast.dart';

class MaxLimitSpeedStreet extends StatefulWidget {
  MaxLimitSpeedStreet({Key key}) : super(key: key);

  @override
  _MaxLimitSpeedStreetState createState() => _MaxLimitSpeedStreetState();
}

class _MaxLimitSpeedStreetState extends State<MaxLimitSpeedStreet> {
  Geolocator geo;
  int speed = 0;
  bool yellowIndicator = false;
  bool redIndicator = false;
  GoogleMapController mapController;
  LatLng _lastMapPosition = _initialPositionLatLng;
  static const _initialPositionLatLng = LatLng(22.7752698, -102.5642267);
  CameraPosition _initialPosition =
      CameraPosition(target: _initialPositionLatLng, zoom: 18.0);
  double latitude, longitude;
  LocationLive current_loc = new LocationLive();
  var _speedLimit = new DataGetApi();
  SpeedMeter _speedMeter;

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  void updateMapFocus(double lat, double long) {
    mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(double.parse(lat.toString()), double.parse(long.toString())),
      zoom: 20.0,
    )));
  }

  @override
  void initState() {
    super.initState();
    current_loc.LocationLiveM();
    current_loc.getStreamCurrentPosition.listen((event) {
      speed = (event.speed * 3.6).toInt();
      updateMapFocus(event.latitude, event.longitude);
    });
    geo = new Geolocator();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: current_loc.getStream, // a Stream<int> or null
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        Widget child;
        Widget childIndicator;

        if (snapshot.hasError) return Text('Error: ${snapshot.error}');
        if (snapshot.connectionState == ConnectionState.none) {
          child = Text(
            "No hay conexión activa actualmente",
            style: TextStyle(
              color: Colors.grey[800],
              fontSize: 15.0,
            ),
          );
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          child = Text(
            "Obteniendo datos del gps...",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.grey[800],
              fontSize: 15.0,
            ),
          );
        } else if (snapshot.connectionState == ConnectionState.active) {
          List<String> temp = snapshot.data.split(",");
          _speedLimit.calculaVelocidadCalle(temp[0], temp[1]);
          _speedLimit.calculateIsBadDirection(null, null);
          child = Text(
            "La velocidad actual en este tramo es: " +
                _speedLimit.getMaxSpeed +
                " KMH",
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 15.0,
            ),
          );

          childIndicator = Text(
              "$speed kmh",
              style: TextStyle(
                color: current_loc.warningAlert(speed, int.parse(_speedLimit.getMaxSpeed)),
                fontWeight: FontWeight.bold,
                fontSize: 25.0,
              ),
            );

          /*if ((int.parse(_speedLimit.getMaxSpeed) - speed) <= 10 &&
              (int.parse(_speedLimit.getMaxSpeed) - speed) > -10) {
            childIndicator = Text(
              "$speed kmh",
              style: TextStyle(
                color: Colors.yellow,
                fontWeight: FontWeight.bold,
                fontSize: 25.0,
              ),
            );
          } else if ((int.parse(_speedLimit.getMaxSpeed) - speed) <= -10) {
            childIndicator = Text(
              "$speed kmh",
              style: TextStyle(
                color: Colors.red,
                fontWeight: FontWeight.bold,
                fontSize: 25.0,
              ),
            );
          } else {
            childIndicator = Text(
              "$speed kmh",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 25.0,
              ),
            );
          }*/
        } else if (snapshot.connectionState == ConnectionState.done) {
          child = Text(
            "Datos obtenidos correctamente",
            style: TextStyle(
              color: Colors.grey[900],
              fontSize: 15.0,
            ),
          );
        }
        return SafeArea(
            child: Scaffold(
          backgroundColor: Colors.grey[400],
          appBar: AppBar(
            centerTitle: true,
            elevation: 2.5,
            title: Text("Reeducavi"),
          ),
          body: Padding(
            padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 20.0),
                  child: Center(child: childIndicator),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 20.0),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        child,
                        _speedLimit.getIsBadDirection ? 
                        Text("Cuidado, sentido incorrecto", style: TextStyle(color: Colors.red),) : 
                        Container()
                      ],
                    ),
                  )
                ),
                Divider(
                  height: 0.0,
                  color: Colors.black,
                  thickness: 2.0,
                ),
                Expanded(
                  child: GoogleMap(
                    onMapCreated: _onMapCreated,
                    initialCameraPosition: _initialPosition,
                    myLocationButtonEnabled: true,
                    myLocationEnabled: true,
                  ),
                ),
              ],
            ),
          ),
        ));
      },
    );
  }
}


