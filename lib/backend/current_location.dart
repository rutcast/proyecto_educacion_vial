import 'dart:async';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class LocationLive {
  StreamController<String> _controller = new StreamController();
  String v = "null";
  String c = "null";

  Geolocator location;
  Stream<Position> locationActual;

  LocationLive() {
    location = new Geolocator();
    locationActual = new Geolocator().getPositionStream();
  }

  Future<List<String>> genLocation() async {
    try {
      location
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
          .then((value) => value);
    } catch (e) {
      print(e);
    }
  }

  //Constructor de la clase que permite, por medio de un timer, elegir cada cuando se quieren realizar las
  //llamadas con la posición actual, esto para no realizar llamadas innecesarias cada segundo a la api
  LocationLiveM() async {
    Timer.periodic(Duration(seconds: 3), (t) {
      //manda a llamar la función para obtener la posición actual en ese momento, una vez que la obtiene,
      //mete el resultado en un stream, el cual es el que será escuchado desde la clase principal
      try {
        location
            .getCurrentPosition(
                desiredAccuracy: LocationAccuracy.bestForNavigation)
            .then((value) {
          v = value.latitude.toString();
          c = value.longitude.toString();
          _controller.sink.add("$v,$c");
        });
        //si no encuentra la posición actual, cerrará el stream inmediatamente
      } catch (e) {
        _controller.close();
      }
    });
  }

  Color warningAlert(int stringSpeed, int stringSpeedStreet) {
    if ((stringSpeedStreet - stringSpeed) <= 10 &&
        (stringSpeedStreet - stringSpeed) > -10) {
      return Colors.yellow;
    } else if ((stringSpeedStreet - stringSpeed) <= -10) {
      return Colors.red;
    } else {
      return Colors.black;
    }
  }

  //regresa el stream de esta clase
  Stream<String> get getStream => _controller.stream;
  Sink<String> get getSink => _controller.sink;

  Stream<Position> get getStreamCurrentPosition => locationActual;

  void set activatePermissions(bool activate) {
    location.forceAndroidLocationManager = activate;
  }

  void stopStream() {
    _controller.close();
  }
}
