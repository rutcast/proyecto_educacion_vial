import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class Notifications {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  Notifications() {
    var initializationSettinsAndroid =
        new AndroidInitializationSettings('ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettinsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

    flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  Future<bool> notificacionExcesoVelocidad() async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        '1',
        'Al pasar limite de velocidad',
        'Esta canal informa cuando has rebasado el limite de velocidad',
        importance: Importance.Max,
        priority: Priority.Max,
        playSound: true,
        largeIcon: DrawableResourceAndroidBitmap('ic_launcher'),
        sound: RawResourceAndroidNotificationSound('alerta_rapido'));
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        1,
        'Alerta de alta velocidad',
        'Tienes una velocidad muy alta para este tramo',
        platformChannelSpecifics);
    return true;
  }

  Future<bool> notificacionVolumenAlto() async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        '2',
        'Al tener el volumen alto',
        'Esta canal informa cuando superas el limite de sonido permitido',
        importance: Importance.Max,
        priority: Priority.Max,
        playSound: true,
        largeIcon: DrawableResourceAndroidBitmap('ic_launcher'),
        sound: RawResourceAndroidNotificationSound('alerta_volumen_alto'));
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    try {
      await flutterLocalNotificationsPlugin.show(2, 'Alerta de volumen alto',
          'Tienes el volumen muy alto', platformChannelSpecifics);
      return true;
    } catch (exception) {
      return false;
    }
  }

  Future<void> notificacionMantenimiento(int dias) async {
    await flutterLocalNotificationsPlugin.cancelAll();
    var scheduledNotificationDateTime = DateTime.now().add(Duration(days: dias));
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        '3',
        'Al llegar la hora de mantenimiento',
        'Este canal informa cuando ha llegado la hora del mantenimiento',
        importance: Importance.Max,
        priority: Priority.Max,
        playSound: true,
        largeIcon: DrawableResourceAndroidBitmap('ic_launcher'),
        sound: RawResourceAndroidNotificationSound('alerta_volumen_alto'));

    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    NotificationDetails platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics, 
      iOSPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.schedule(
      3,
      'Mantenimiento',
      'Es hora de darle mantenimiento a tu auto',
      scheduledNotificationDateTime,
      platformChannelSpecifics,androidAllowWhileIdle: true);
  }

  void cancelNotification() {
    flutterLocalNotificationsPlugin.cancel(1);
  }
}
