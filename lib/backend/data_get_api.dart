import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_compass/flutter_compass.dart';

class DataGetApi {
  String _maxSpeed = "0";
  bool _isBadDirection = false;
  List _travelDirection = [];
  Map<String, String> _velocity_category_speed = {
    "SC8": "20",
    "SC7": "30",
    "SC6": "40",
    "SC5": "60",
    "SC4": "80",
    "SC3": "100",
    "SC2": "120",
    "SC1": "130"
  };

  //llave de acceso para usar la api de here maps
  String key = "n8aWg1qu5XWTdIX4qYTA-T44W9dviJyiEpAcYqhTsWk";

  //función que manda a llamar el calculo de la velocidad por calle
  Future<String> calculaVelocidadCalle(String latitud, String longitud) async =>
      calculaPlaceId(latitud, longitud);

  //función que calcula la velocidad por calle basandose en la latitud y longitud actual
  Future<String> calculaPlaceId(String latitud, String longitud) async {
    try {
      final response = await http.get(
          'https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$latitud,$longitud,100&mode=retrieveAddresses&maxresults=1&locationattributes=linkInfo&apiKey=$key');
      // print('https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$latitud,$longitud,100&mode=retrieveAddresses&maxresults=1&locationattributes=linkInfo&apiKey=$key');
      final consulta = json.decode(response.body);
      // print(consulta);
      final reduct = consulta['Response']['View'][0]['Result'][0]['Location']
          ['LinkInfo']['SpeedCategory'];
      _maxSpeed = _velocity_category_speed[reduct];
      _travelDirection = consulta['Response']['View'][0]['Result'][0]['Location']
          ['LinkInfo']['TravelDirection'];
      return _velocity_category_speed[reduct];
    } catch (e) {
      _maxSpeed = "0";
      return "No fue posible obtener el limite de velocidad en este segmento";
    }
  }

  Future<bool> calculateIsBadDirection(double direction, List directionAllowed) async {
    if(direction == null) direction = await FlutterCompass.events.first;
    if(directionAllowed == null) directionAllowed = _travelDirection;
    // print(directionAllowed);
    List coordNames = ["N", "NE", "E", "SE", "S", "SW", "W", "NW", "N"];
    int coordIndex = (direction / 45).round();
    if (coordIndex < 0) coordIndex = coordIndex + 8;
    // print(coordNames[coordIndex]);
    return !directionAllowed.contains(coordNames[coordIndex]);
  }

  //getter que permite regresar la velocidad actual una vez obtenida
  String get getMaxSpeed => _maxSpeed;
  List get travelDirection => _travelDirection;
  bool get getIsBadDirection => _isBadDirection;

}
